package jp.alhinc.ito_fumiya.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class CalculateSales2 {
	public static void main(String[] args) {
		//Mapの作成
		HashMap<String,String> difinitionMap = new HashMap<String, String>();
		HashMap<String, Long> salesMap = new HashMap<String, Long>();
		BufferedReader br = null;

		//コマンドライン引数チェック
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		//inputメソッドの呼び出し
		if(!input(args[0],"branch.lst",difinitionMap,salesMap, "支店")) {

			return;
		}

		//ファイルの入っているディレクトリを取得
		File dir = new File(args[0]);

		//listFilesメソッドを使用して一覧を取得
		File[] allFile = dir.listFiles();

		//該当ファイルのリストの宣言
		List<String> salesFileList = new ArrayList<String>();

		//ファイル名だけを取り出し、入れるリスト
		List<String> fileNameList = new ArrayList<String>();

		//売り上げファイルをリストに入れる
		for(int i=0; i<allFile.length; i++) {
			if(allFile[i].isFile() && allFile[i].getName().matches("^[0-9]{8}(.rcd)$")) {
				salesFileList.add(allFile[i].getName());
			}
		}
		//連番チェック
		//売り上げファイルの中から8文字だけを取り出しリストに入れる
		for(int i = 0; i < salesFileList.size(); i++) {
			fileNameList.add(salesFileList.get(i).substring(0,8));
		}
		//売り上げファイルを順番に計算して差が1ずつかをチェック
		for(int i = 0; i < fileNameList.size() -1 ;i++) {
			int x1 = Integer.parseInt(fileNameList.get(i));
			int x2 = Integer.parseInt(fileNameList.get(i + 1));

			if(x2 - x1 != 1 ) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		//読み込んだ売り上げファイルをひとつずつリーダーで読み込む
		for(int i = 0; i < salesFileList.size(); i++) {
			try {
				//String型で読み込んだファイルをファイル型に変換
				File file = new File(args[0],salesFileList.get(i));
				FileReader fr = new FileReader(file);
				br = new BufferedReader(fr);


				//売り上げファイルの中身を入れるリスト
				ArrayList<String> salesFileContent = new ArrayList<String>();
				String line;
				//行が空になるまでファイルの中身を読み込む
				while ((line = br.readLine()) != null){
					salesFileContent.add(line);
				}

				//売り上げファイルの行数の判定
				if(salesFileContent.size() != 2) {
					System.out.println(salesFileList.get(i) + "のフォーマットが不正です");
					return;
				}

				//売り上げファイルの金額の判定
				if(!salesFileContent.get(1).matches("[0-9]*")) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}

				//String型「shopCode」にリストの0の店舗コードを代入
				String shopCode = salesFileContent.get(0);

				//整数型「sale」に、リストの１である売り上げを代入
				long sale = Long.parseLong(salesFileContent.get(1));


				//売り上げファイルの支店コードが存在しない場合の処理
				if(!difinitionMap.containsKey(shopCode)){
					System.out.println(salesFileList.get(i) + "の支店コードが不正です");
					return;
				}

				//合計金額が10桁を超えるかどうかの判定
				Long total = salesMap.get(shopCode) + sale;
				if(total > 9999999999L) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				//売り上げファイルの金額を支店コード毎に合算
				salesMap.put(shopCode, salesMap.get(shopCode) + sale);
			} catch(IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;
			} finally {
				if(br != null) {
					try {
						br.close();
					} catch(IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}
		if(!output(args[0],"branch.out",difinitionMap,salesMap)) {
			return;
		}

	}

	//outputメソッド。booleanで値を返す。引数に出力に必要なものを指定する。
	public static boolean output(String directryPath, String fileName,HashMap<String,String> difinitionMap, HashMap<String, Long> salesMap ) {
		BufferedWriter bw = null;
		try {
			File file = new File(directryPath, fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);

			//拡張for文（mapのkeysetメソッドでまわす）
			String lineSepa = System.getProperty("line.separator");
			for(String order : difinitionMap.keySet()) {
				bw.write(order + "," + difinitionMap.get(order) + "," + salesMap.get(order) + lineSepa);
			}
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		} finally {
			if(bw != null) {
				try {
					bw.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
				}
			}
		}
		return true;
	}

	//inputメソッド
	public static boolean input(String directoryPath,String fileName,HashMap<String,String> difinitionMap, HashMap<String,Long> salesMap,String difinitionType) {
		BufferedReader br = null;
		try {
			//支店定義ファイル読み込み
			File file = new File(directoryPath, fileName);

			//支店定義ファイルがあるかどうか
			if(!file.exists()) {
				System.out.println(difinitionType + "定義ファイルが存在しません");
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);
			String line;

			while((line = br.readLine()) != null) {
				//文字列をカンマで分割
				String[] names = line.split(",");

				//支店コードが3桁の整数でない場合、又は要素数が2ではない場合
				if(!names[0].matches("[0-9]{3}") || names.length != 2) {
					System.out.println(difinitionType + "定義ファイルのフォーマットが不正です");
					return false;
				}
				//difinitionMapに支店コード、支店名を入れる
				difinitionMap.put(names[0], names[1]);
				salesMap.put(names[0],0L);
			}
		} catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました。");
			return false;
		} finally {
			if(br != null) {
				try {
					br.close();
				} catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
				}
			}
		}
		return true;
	}

}

